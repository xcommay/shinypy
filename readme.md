#ShinyPy
### python to shiny bridge


ShinyPy is a hack which uses the excellent R [shiny] (http://shiny.rstudio.com/) package to add interfaces to python functions. The converter.py script reads a given function and a configuration dictionary and creates ui.R and server.R scripts so that R calles the python function through [rPython] (http://rpython.r-forge.r-project.org/).

## Requirements
Your R installation must have shiny and rPython already installed. 

## Example 1 (example1.py)

~~~python
def sum(x=1, y=2):
	"""
	adds x and y together
	
	#shinypy
	
	{
	'x':{'type': 'numericInput', 'label':'add value of x here',  'value':1}, 
	'y':{'type': 'numericInput', 'label':'add value of y here', 'value':10},
	'z':{'type':'textOutput'},
	'_shinyTitle_':'The adding machine',
	'_output_':['z']
	}
	
	#shinypy
	"""
	z = x + y
	return z


~~~



~~~
#python converter.py <py script name> <function name>

python converter.py example1.py sum

#output 
dyn-118-139-104-89:auto wishvah$ python converter.py example1.py sum

Loading required package: shiny
Loading required package: methods

Listening on http://127.0.0.1:3515

~~~

Then go to http://127.0.0.1:3515 and you will see the following

![example1_screenshot](https://bitbucket.org/xcommay/shinypy/raw/master/example1_screenshot.png)


## Example 2 - working with pandas dataframes and file uploads (example2.py)

~~~python
import pandas as pd
def display_dataframe(f):
    """
    adds a and b together

    #shinypy
    
    {
    'f': {'type':'fileInput', 'label' : 'value for b'},
    'df':{'type':'dataTableOutput'},
    '_shinyTitle_':'Display pandas dataframe',
    '_output_':['df']
   
   }
    #shinypy
    """

    import pandas
    df = pandas.read_csv(f)
    print df.columns
    print df
    df2 = df.to_dict(orient="list")
    return df2


~~~

##Output
![example2_screenshot](https://bitbucket.org/xcommay/shinypy/raw/master/example2_screenshot.png)

## Example 3 - displaying a matplotlib image (example3.py)

~~~python 
import pandas as pd
def draw_plot_from_dataframe(f):
    """
    adds a and b together

    #shinypy
    
    {
    'f': {'type':'fileInput', 'label' : 'file name'},
    'plot':{'type':'imageOutput'},
    '_shinyTitle_':'Display pandas plot',
    '_output_':['plot']
   
   }
    #shinypy
    """

    import pandas
    import matplotlib.pyplot as plt
    df = pandas.read_csv(f,names=["one","two","three"])
    p = df['two'].hist()
    plt.savefig("test.png")
    plot = "test.png"
    return plot
    
~~~


## Output
* note * In OS X this results in a matplotlib specific error. But you can simply import the generated ui and server.R files into R / R studio and then run Shiny and it will work fine.

![example3_screenshot](https://bitbucket.org/xcommay/shinypy/raw/master/example3_screenshot.png)

#Notes

* make sure you correctly type in the config dictionary (between two #shinypy tags)
* the config dictionary string should result in a wellformed dict when passed through ast.literaleval()
* Currently supported inputs are
	* fileInput
	* numericInput
	* textInput
* Currently supported output are
	* imageOutput
	* textOutput
	* tableOutput
* The function can have multple outputs - in such cases make sure return a list of variables.
* To return a pandas dataframe make sureyou df.to_dict(orient="list") so that it can be properly serialized by rPython. 
* For all shiny input and output emthods(imageOutput etc) you can specify native R options (e.g. min for numericInput in the config dict as an additional key-value pair.
* For rendering options (e.g renderTable, add the R native arguments as a seperate dictionary with the key value of 'render'





