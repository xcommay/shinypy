def sum(x=1, y=2):
	"""
	adds x and y together
	
	#shinypy
	
	{
	'x':{'type': 'numericInput', 'label':'add value of x here',  'value':1}, 
	'y':{'type': 'numericInput', 'label':'add value of y here', 'value':10},
	'z':{'type':'textOutput', 'h':'h1'},
	'_shinyTitle_':'The adding machine',
	'_output_':['z']
	}
	
	#shinypy
	"""
	z = x + y
	return z
