import pandas as pd
def draw_plot_from_dataframe(f):
    """
    adds a and b together

    #shinypy
    
    {
    'f': {'type':'fileInput', 'label' : 'file name'},
    'plot':{'type':'imageOutput'},
    '_shinyTitle_':'Display pandas plot',
    '_output_':['plot']
   
   }
    #shinypy
    """

    import pandas
    import matplotlib.pyplot as plt
    df = pandas.read_csv(f,names=["one","two","three"])
    p = df['two'].hist()
    plt.savefig("test.png")
    plot = "test.png"
    return plot
    
