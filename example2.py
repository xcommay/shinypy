import pandas as pd
def display_dataframe(f):
    """
    adds a and b together

    #shinypy
    
    {
    'f': {'type':'fileInput', 'label' : 'value for b'},
    'df':{'type':'dataTableOutput'},
    '_shinyTitle_':'Display pandas dataframe',
    '_output_':['df']
   
   }
    #shinypy
    """

    import pandas
    df = pandas.read_csv(f)
    print df.columns
    print df
    df2 = df.to_dict(orient="list")
    return df2
