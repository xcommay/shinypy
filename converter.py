#creates a shiny app using a python function. 
#input python converter.py python_file function_name


ui_template_string = """
library(shiny)
shinyUI(fluidPage(
titlePanel("$title"),
sidebarLayout(
    sidebarPanel(
      $input_chunk
      ),
    mainPanel(
      $output_chunk
    ))))"""
    

server_template_string = """
library(shiny)
library(rPython)
python.load('$python_file')
shinyServer(function(input, output) {
d <- reactive({
    d <- python.call('$function_name', $inputs)
  })
   
$output_chunk
  
 
})"""

import sys
import inspect
import ast
import string
from collections import OrderedDict
import copy
import os


python_file = sys.argv[1].strip().replace(".py", "")
function_name = sys.argv[2].strip()

module = __import__(python_file)
f = getattr(module, function_name)

fi = inspect.getargspec(f)

args = fi.args
defaults = fi.defaults

"""if len(args) != len(defaults):
    print "Error make sure all arguments have default values"
    sys.exit(1)
"""

#print args, defaults

#extracting variable types
config_text = f.__doc__.strip().split('#shinypy')[1].replace('\n','').strip()

config_dict = ast.literal_eval(config_text)
#print config_dict



def convert(args, config_dict):

    config_dict2 = copy.deepcopy(config_dict)
    #working on ur.R
   
    server = open("server.R", "w")
    
    #generating ui.R - need to fil title, input_chunk and output_chunk
    
    if '_shinyTitle_' in config_dict.keys():
        title = config_dict['_shinyTitle_']
    else:
        title = "Your title goes here!"
        
    #generating input_chunk
    input_chunk = ""
    for var in args:
        #find type
        var_dict = config_dict[var] 
        Rtype = var_dict['type']
        del var_dict['type']   
        
        #compiling other arguments
        keys = var_dict.keys()
        arg_string = ""
        for k in keys:
            if type(var_dict[k]) == str:
                arg_string = arg_string + ',' + k + '="' + str(var_dict[k]) + '"'
            else:
                #assuming its a number
                arg_string = arg_string + ',' + k + '=' + str(var_dict[k]) 
                                
            

        input_chunk =  input_chunk +  "%s('%s' %s )," % (Rtype, var, arg_string)
        arg_string = ""

    
    input_chunk = input_chunk[:-1] #removes the trailing comma
    
    #generating output_chunk
    output_chunk = ""

    #identify outputs : outputs are not in args and do not begin and end with a "_"

    io = config_dict.keys()
    for var in io:
        if (var not in args) and (var[0] != "_") and (var[-1] != "_") :
            #then var is an output variable.
            var_dict = config_dict[var]
            #see if h decorator is defined
            Rtype = var_dict['type']
            del var_dict['type']

            keys = var_dict.keys()
            arg_string = ""
            for k in keys:
                if k in ['h','render']: #this is the exclusion list
                    continue
                if type(var_dict[k]) == str:
                    arg_string = arg_string + ',' + k + '="' + str(var_dict[k]) + '"'
                else:
                    #assuming its a number
                    arg_string = arg_string + ',' + k + '=' + str(var_dict[k]) 
                    
            #work on the exclusion list
            if 'h' in keys:
                output_chunk =  output_chunk +  "%s(%s('%s' %s ))," % (var_dict['h'], Rtype, var, arg_string)
            else:
                output_chunk =  output_chunk +  "%s('%s' %s )," % (Rtype, var, arg_string)
            
            
    output_chunk = output_chunk[:-1]
    
    ui = open("ui.R", "w")
    uiT = string.Template(ui_template_string)
    output = uiT.substitute(title = title, input_chunk = input_chunk, output_chunk = output_chunk)
    ui.write(output)
    ui.close()
    
    
    ui.close()
    
    #working on server.R - need to fill python_file, function_name, inputs and output_chunk
    
    python_file = sys.argv[1].strip()
    function_name =  sys.argv[2].strip()
    
    #generating inputs 
    
    #args contains the input variable names in the order they appear
    #we are using the same arg variable names in ur.R
    
    inputs = ""
    for var in args:
        if config_dict2[var]['type'] == 'fileInput':
            inputs = inputs + "input$%s$datapath," % (var)
        else:
            inputs = inputs + "input$%s," % (var)
        
    inputs = inputs[:-1] #remove trailing comma
    
    #generating output chunk
    print config_dict2
    
    def ui_to_server_output_convert(ui_type):
        """
        imageOutput -> renderImage
        textOutput -> renderText
        tableOutput -> renderTable
        dataTableOutput -> renderDataTable
        """
        
        if ui_type == 'imageOutput':
            return 'renderImage'
        elif ui_type == 'textOutput':
            return 'renderText'
        elif ui_type == 'tableOutput':
            return 'renderTable'
        elif ui_type == 'dataTableOutput':
            return 'renderDataTable'
            
    #the key '_output_'  contains the list out outputs in the same order as the function.
    outputs = config_dict2['_output_']
    
    count = 0
    output_chunk = ""
    
    for out_var in outputs:
        count = count + 1
        #getType
        var_dict = config_dict2[out_var]
        print out_var, var_dict
        Rtype = var_dict['type']
        render_type = ui_to_server_output_convert(Rtype)
        
        #adding options to the render function
        arg_string = ""
        if 'render' in var_dict.keys():
            render = var_dict['render']
            for k in render.keys():
                if type(var_dict[k]) == str:
                    arg_string = arg_string + ',' + k + '="' + str(var_dict[k]) + '"'
                else:
                    #assuming its a number
                    arg_string = arg_string + ',' + k + '=' + str(var_dict[k]) 
                    
        if render_type == "renderTable" or render_type == "renderDataTable":
            if len(outputs) == 1:
                output_chunk = output_chunk + "output$%s <- %s(data.frame(d())  %s)" % (out_var, render_type, arg_string) + "\n"

            else:
                output_chunk = output_chunk + "output$%s <- %s(data.frame(d()[[%s]])  %s)" % (out_var, render_type, count, arg_string) + "\n"
        
        elif render_type == "renderImage":
            output_chunk = output_chunk + "output$%s <- %s(list(src=d()[[%s]])  %s)" % (out_var, render_type, count, arg_string) + "\n"

        
        else:
            output_chunk = output_chunk + "output$%s <- %s(d()[[%s]]  %s)" % (out_var, render_type, count, arg_string) + "\n"
            
      
            
    
    server = open("server.R" , "w")
    serverT = string.Template(server_template_string)
    output = serverT.substitute(python_file = python_file, function_name = function_name, inputs = inputs, output_chunk=output_chunk)
    server.write(output)
    server.close()
        
    rFile = open("runMe.R","w")
    rFile.write("shiny::runApp('.')")
    rFile.close()
    os.system("Rscript runMe.R")
    
    
    
    
    
    
                
    


convert(args,config_dict)
    
